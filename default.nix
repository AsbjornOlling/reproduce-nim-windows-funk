{ lib, stdenv, buildNimPackage }:

buildNimPackage {
  name = "reproduce funky problem";
  src = ./.;
  nimFlags = lib.optionals stdenv.hostPlatform.isMinGW ["--os:windows" "-d:mingw"];
  meta = { platforms = lib.platforms.all; };
}
