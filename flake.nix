{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:AsbjornOlling/nixpkgs/fix/nim-cyclic-windows-build";
    # 'windows' cross-compile target does not work when using nixpkgs
    # nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs { system = "x86_64-linux"; };
  in
  {
    packages.x86_64-linux.default = pkgs.callPackage (./.) {};
    packages.x86_64-linux.windows = pkgs.pkgsCross.mingwW64.callPackage (./.) {};
  };
}
