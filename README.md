# Ok so...

Cross compiling to windows using nixpkgs `buildNimPackage` is broken.

`nix build .` (linux -> linux build) works fine

`nix build '.#windows'` does not work (if the `os` package is imported)

It seems like the nixbuild patch in nixpkgs' buildNimPackage adds an `import os`, which results in an import loop when building for windows.
