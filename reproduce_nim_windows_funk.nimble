# Package

version       = "0.1.0"
author        = "AsbjornOlling"
description   = "A new awesome nimble package"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["reproduce_nim_windows_funk"]


# Dependencies

requires "nim >= 2.0.4"
